package fr.eql.ai110.spaceplanner.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.spaceplanner.model.Statement;
import fr.eql.ai110.spaceplanner.model.Team;
import fr.eql.ai110.spaceplanner.model.User;
import fr.eql.ai110.spaceplanner.service.AccountIBusiness;
import fr.eql.ai110.spaceplanner.service.StatementIBusiness;
import fr.eql.ai110.spaceplanner.service.TeamIBusiness;

@Controller(value = "mbTeam")
@Scope(value = "session")
public class TeamManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private User employee;
	private Set<User> employeesInTeam;
	private User manager;
	private Team team;
	private User connectedUser;
	private Set<Team> teamsByManager;

	private Statement statement;
	private LocalDate selectedDay;
	private Set<Statement> statementsByEmployee;
	private Set<Statement> statementsByManager;

	LocalDate today = LocalDate.now();
	private String mon;
	private String tue;
	private String wed;
	private String thu;
	private String fri;
	private String shortMon;
	private String shortFri;
	private LocalDate firstDay;
	private LocalDate secondDay;
	private LocalDate thirdDay;
	private LocalDate fourthDay;
	private LocalDate fifthDay;
	
	private int weekNumber;
	private String month;

	@PostConstruct
	public void intit()
	{
		formatDates();	
	}

	@Autowired
	private TeamIBusiness teamBusiness;
	@Autowired
	private AccountIBusiness accountBusiness;
	@Autowired
	private StatementIBusiness statementBusiness;

	public void formatDates()
	{
		WeekFields weekFields = WeekFields.of(Locale.FRENCH); 
		TemporalField fieldISO = weekFields.dayOfWeek();
		
		this.firstDay = today.with(fieldISO, 1);
		this.secondDay = today.with(fieldISO, 2);
		this.thirdDay = today.with(fieldISO, 3);
		this.fourthDay = today.with(fieldISO, 4);
		this.fifthDay = today.with(fieldISO, 5);
		this.weekNumber = today.get(weekFields.weekOfWeekBasedYear());
		this.shortMon = firstDay.format(DateTimeFormatter.ofPattern("dd MMMM", Locale.FRENCH));
		this.shortFri = fifthDay.format(DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.FRENCH));
		this.mon = firstDay.format(DateTimeFormatter.ofPattern("EEE dd/MM", Locale.FRENCH));
		this.tue = secondDay.format(DateTimeFormatter.ofPattern("EEE dd/MM", Locale.FRENCH));
		this.wed = thirdDay.format(DateTimeFormatter.ofPattern("EEE dd/MM", Locale.FRENCH));
		this.thu = fourthDay.format(DateTimeFormatter.ofPattern("EEE dd/MM", Locale.FRENCH));
		this.fri = fifthDay.format(DateTimeFormatter.ofPattern("EEE dd/MM", Locale.FRENCH));	
	}
	
	public void resetToday()
	{
		this.today = LocalDate.now();
	}
	
	@PostConstruct
	public void displayTeamsByManager()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		connectedUser = (User) session.getAttribute("connectedUser");
		//retrieve the list of managed teams by connected user (manager)
		teamsByManager = teamBusiness.getManagedTeams(connectedUser);
	}

	public Long fetchNbTeamsByManager(User manager)
	{
		return teamBusiness.getNbTeamsByManager(manager);
	}

	public String displayTeamDetails(Team selectedTeam)
	{
		String forward = null;
		this.team = selectedTeam;
		this.employeesInTeam = accountBusiness.getAllEmployeesInTeam(selectedTeam);
		forward = "/teamdashboard.xhtml?faces-redirect=true";
		
		resetToday();
		formatDates();
		return forward;
	}
	
	public Set<Statement> fetchValidatedStatementsByEmployee(User employee, LocalDate minDate, LocalDate maxDate)
	{
		minDate = firstDay;
		maxDate = fifthDay;
//		return statementBusiness.getValidatedAttendanceDates(employee);
		statementsByEmployee =  statementBusiness.getValidatedAttendanceDatesBetweenDates(employee, minDate, maxDate);

		return statementsByEmployee;
	}
	
	public Set<LocalDate> retrieveValidatedStatementsDates(Set<Statement> selectedValidatedStatements)
	{
		Set<LocalDate> validatedStatementsDates = new HashSet<LocalDate>(); 
		
		for (Statement statement : selectedValidatedStatements)
		{
			validatedStatementsDates.add(statement.getSelectedDay());
		}
		return validatedStatementsDates;
	}

	public boolean isPresentOnGivenDay(Set<LocalDate> vSDates, LocalDate givenDay)
	{
		boolean confirmedAttendance = false;

		for (LocalDate selectedDay : vSDates)
		{		
			if (selectedDay.compareTo(givenDay)==0)
			{
				confirmedAttendance = true;	
			break;
			}	
		}		
		return confirmedAttendance;
	}
	
	public String displayNextWeek()
	{
		String forward = null;
		this.today = today.plusDays(7);
		forward = "/teamdashboard.xhtml?faces-redirect=true";
		
		formatDates();
		return forward;
	}
	
	public String displayPreviousWeek()
	{
		String forward = null;
		this.today = today.minusDays(7);;
		forward = "/teamdashboard.xhtml?faces-redirect=true";
		
		formatDates();
		return forward;
	}
		
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public User getEmployee()
	{
		return employee;
	}
	public void setEmployee(User employee)
	{
		this.employee = employee;
	}
	public Set<User> getEmployeesInTeam()
	{
		return employeesInTeam;
	}
	public void setEmployeesInTeam(Set<User> employeesInTeam)
	{
		this.employeesInTeam = employeesInTeam;
	}
	public User getManager()
	{
		return manager;
	}
	public void setManager(User manager)
	{
		this.manager = manager;
	}
	public Team getTeam()
	{
		return team;
	}
	public void setTeam(Team team)
	{
		this.team = team;
	}
	public User getConnectedUser()
	{
		return connectedUser;
	}
	public void setConnectedUser(User connectedUser)
	{
		this.connectedUser = connectedUser;
	}
	public Set<Team> getTeamsByManager()
	{
		return teamsByManager;
	}
	public void setTeamsByManager(Set<Team> teamsByManager)
	{
		this.teamsByManager = teamsByManager;
	}
	public Statement getStatement()
	{
		return statement;
	}
	public void setStatement(Statement statement)
	{
		this.statement = statement;
	}
	public LocalDate getSelectedDay()
	{
		return selectedDay;
	}
	public void setSelectedDay(LocalDate selectedDay)
	{
		this.selectedDay = selectedDay;
	}
	public Set<Statement> getStatementsByEmployee()
	{
		return statementsByEmployee;
	}
	public void setStatementsByEmployee(Set<Statement> statementsByEmployee)
	{
		this.statementsByEmployee = statementsByEmployee;
	}
	public Set<Statement> getStatementsByManager()
	{
		return statementsByManager;
	}
	public void setStatementsByManager(Set<Statement> statementsByManager)
	{
		this.statementsByManager = statementsByManager;
	}
	public int getWeekNumber()
	{
		return weekNumber;
	}
	public void setWeekNumber(int weekNumber)
	{
		this.weekNumber = weekNumber;
	}
	public String getMon()
	{
		return mon;
	}
	public void setMon(String mon)
	{
		this.mon = mon;
	}
	public String getTue()
	{
		return tue;
	}
	public void setTue(String tue)
	{
		this.tue = tue;
	}
	public String getWed()
	{
		return wed;
	}
	public void setWed(String wed)
	{
		this.wed = wed;
	}
	public String getThu()
	{
		return thu;
	}
	public void setThu(String thu)
	{
		this.thu = thu;
	}
	public String getFri()
	{
		return fri;
	}
	public void setFri(String fri)
	{
		this.fri = fri;
	}
	public LocalDate getFirstDay() {
		return firstDay;
	}

	public void setFirstDay(LocalDate firstDay)
	{
		this.firstDay = firstDay;
	}
	public LocalDate getSecondDay()
	{
		return secondDay;
	}
	public void setSecondDay(LocalDate secondtDay)
	{
		this.secondDay = secondtDay;
	}
	public LocalDate getThirdDay()
	{
		return thirdDay;
	}
	public void setThirdDay(LocalDate thirdDay)
	{
		this.thirdDay = thirdDay;
	}
	public LocalDate getFourthDay()
	{
		return fourthDay;
	}
	public void setFourthDay(LocalDate fourthDay)
	{
		this.fourthDay = fourthDay;
	}
	public LocalDate getFifthDay()
	{
		return fifthDay;
	}
	public void setFifthDay(LocalDate fifthDay)
	{
		this.fifthDay = fifthDay;
	}
	public String getShortMon()
	{
		return shortMon;
	}
	public void setShortMon(String shortMon)
	{
		this.shortMon = shortMon;
	}
	public String getShortFri()
	{
		return shortFri;
	}
	public void setShortFri(String shortFri)
	{
		this.shortFri = shortFri;
	}
	public String getMonth()
	{
		return month;
	}
	public void setMonth(String month)
	{
		this.month = month;
	}
	public LocalDate getToday()
	{
		return today;
	}
	public void setToday(LocalDate today)
	{
		this.today = today;
	}

}
