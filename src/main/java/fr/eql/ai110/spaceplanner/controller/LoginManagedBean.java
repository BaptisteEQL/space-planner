package fr.eql.ai110.spaceplanner.controller;

import java.io.Serializable;
import java.time.LocalDate;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.spaceplanner.model.Team;
import fr.eql.ai110.spaceplanner.model.User;
import fr.eql.ai110.spaceplanner.service.AccountIBusiness;

@Controller(value="mbLogin")
@Scope(value="request")
public class LoginManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private String surname;
	private String email;
	private String login;
	private String password;
	private LocalDate inscriptionDate;
	private User user;
	private Team team;
	private boolean connected;

	@Autowired
	private AccountIBusiness accountBusiness;

	@PostConstruct
	public void init()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		// retrieve the current session
		HttpSession session = (HttpSession) facesContext.getExternalContext()
				.getSession(false);
		user = (User) session.getAttribute("connectedUser");
	}

	public String signIn()
	{
		String forward = null;
		user = accountBusiness.hashedEncryptionConnect(login, password);
		if (user != null)
		{
			// the method found the user in the DB and redirect to index in "connected mode"
			// connected attribute will give info on what to display on different xhtml pages 
			connected = true;
			FacesContext facesContext = FacesContext.getCurrentInstance();
			// retrieve the current session
			HttpSession session = (HttpSession) facesContext.getExternalContext()
					.getSession(false);
			session.setAttribute("connectedUser", user);
			forward = "/account.xhtml?faces-redirect=true";
		}
		else
		{
			// the method didn't found the user in the DB and keeps the user on login page
			// connected attribute will give info on what to display on different xhtml pages 
			connected = false;
			FacesMessage facesMessage = new FacesMessage
					(FacesMessage.SEVERITY_WARN, 
							"Identifiant et/ou Mot de passe incorrect(s)", ""
							);
			FacesContext.getCurrentInstance().addMessage("encyptedLoginForm:inputLogin", facesMessage);
			FacesContext.getCurrentInstance().addMessage("encyptedLoginForm:inputPassword", facesMessage);
			forward = "/signin.xhtml?faces-redirect=false";
		}
		return forward;
	}

	public boolean isConnected()
	{
		return user != null;
	}

	public String disconnect() {
		// connected attribute will give info on what to display on different xhtml pages 
		connected = false;
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		session.invalidate();
		return "/signin.xhtml?faces-redirect=true";
	}

	public String signUp()
	{
		String forward = null;
		user = new User(null, name, surname, email, inscriptionDate, login, 
				password, null, null, null, null, null);
		// the method look for existing login in the DB
		// create the user if login doesn't already exist
		// and redirect to index in "connected mode"
		if(accountBusiness.createNewUserHashed(user))
		{
			forward = "/account.xhtml?faces-redirect=true";
		}
		else
		{
			FacesMessage facesMessage = new FacesMessage
					(FacesMessage.SEVERITY_WARN,"Le login existe déjà",""
							);
			FacesContext.getCurrentInstance().addMessage("newAccountForm:inputLogin", facesMessage);
			forward = "/signup.xhtml?faces-redirect=false";
		}
		return forward;
	}

	//employees in the team managed by the current user
	public Long fetchNbEmployeesInTeam(Team team)
	{
		return accountBusiness.getNbEmployeesInTeam(team);
	}
	
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getSurname()
	{
		return surname;
	}
	public void setSurname(String surname)
	{
		this.surname = surname;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public String getLogin()
	{
		return login;
	}
	public void setLogin(String login)
	{
		this.login = login;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public LocalDate getInscriptionDate()
	{
		return inscriptionDate;
	}
	public void setInscriptionDate(LocalDate inscriptionDate)
	{
		this.inscriptionDate = inscriptionDate;
	}
	public User getUser()
	{
		return user;
	}
	public void setUser(User user)
	{
		this.user = user;
	}
	public Team getTeam()
	{
		return team;
	}
	public void setTeam(Team team)
	{
		this.team = team;
	}

}
