package fr.eql.ai110.spaceplanner.service;

import java.util.Set;

import fr.eql.ai110.spaceplanner.model.Team;
import fr.eql.ai110.spaceplanner.model.User;

public interface AccountIBusiness {

	public User connect(String login, String password);
	public User hashedEncryptionConnect(String login, String password);
	public Set<User> getAllUsers();
	public Set<User> getAllEmployeesInTeam(Team team);
	public Long getNbEmployeesInTeam(Team team);
	public User getTeamManager(Team team);
	public boolean createNewUserHashed(User user);
	public String hashedPassword(String password);
	public String saltGenerator();
}
