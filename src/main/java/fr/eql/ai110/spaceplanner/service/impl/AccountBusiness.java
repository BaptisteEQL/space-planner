package fr.eql.ai110.spaceplanner.service.impl;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.spaceplanner.model.Team;
import fr.eql.ai110.spaceplanner.model.User;
import fr.eql.ai110.spaceplanner.repository.UserIDao;
import fr.eql.ai110.spaceplanner.service.AccountIBusiness;

@Service
public class AccountBusiness implements AccountIBusiness {

	@Autowired
	private UserIDao userDao;
	private User user;
	
	@Override
	public User connect(String login, String password)
	{
		return userDao.findByLoginAndPassword(login, password);
	}

	@Override
	public User hashedEncryptionConnect(String login, String password)
	{
		user = userDao.findByLogin(login);
		if(user != null)
		{
			password = password + user.getSalt();
			user = userDao.findByLoginAndPassword(login, hashedPassword(password));
		}
		return user;
	}
	
	@Override
	public Set<User> getAllUsers()
	{
		return (Set<User>) userDao.findAll();
	}

	@Override
	public Set<User> getAllEmployeesInTeam(Team team)
	{
		return userDao.findByTeam(team);
	}
	
	//retrieve the number of users (identified as "employees") in the team 
	//managed by the current user ("manager")
	@Override
	public Long getNbEmployeesInTeam(Team team)
	{
		return userDao.countByTeam(team);
	}

	@Override
	public User getTeamManager(Team team)
	{
		return userDao.findByTeams(team);
	}
	
	@Override
	public boolean createNewUserHashed(User user)
	{
		boolean isCreated = false;
		user.setSalt(saltGenerator());
		//add salt to the hash
		user.setPassword(user.getPassword() + user.getSalt());
		user.setPassword(hashedPassword(user.getPassword()));
		//business rule to check login uniqueness
		if (userDao.findByLogin(user.getLogin()) == null)
		{
			userDao.save(user);
			isCreated = true;
		}
		return isCreated;
	}

	@Override
	public String hashedPassword(String password)
	{

		try
		{
			//choose of hash algorithm with SHA-256
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			//encoding the password into a byte array
			byte[] encodedHash = md.digest(password.getBytes(StandardCharsets.UTF_8));
			//new password string generation using the byte array
			password = new String(encodedHash, StandardCharsets.UTF_8);
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return password;
	}

	@Override
	public String saltGenerator()
	{
		List<Integer> alphanumericList = new ArrayList<>();
		Random rand = new Random();
		String salt = "";

		for (int i = 0; i < 3; i++)
		{
			//add alphanumeric chars to the list
			alphanumericList.add((rand.nextInt(10) + 48));
			alphanumericList.add((rand.nextInt(26) + 65));
			alphanumericList.add((rand.nextInt(26) + 97));

			//get random char from the list
			int randIndexList = rand.nextInt(alphanumericList.size());
			int randChar = alphanumericList.get(randIndexList);

			char c = (char)randChar;
			//add random char from alphanumeric list to salt
			salt += c;
		}
		return salt;
	}

}
