package fr.eql.ai110.spaceplanner.service;

import java.util.Set;

import fr.eql.ai110.spaceplanner.model.Team;
import fr.eql.ai110.spaceplanner.model.User;

public interface TeamIBusiness {

	public Team getByName(String teamName);
	public Team getTeamByEmployee(User employee);
	public Long getNbTeamsByManager(User manager);
	public Set<Team> getManagedTeams(User connectedManager);
}
