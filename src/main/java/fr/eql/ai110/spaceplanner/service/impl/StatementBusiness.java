package fr.eql.ai110.spaceplanner.service.impl;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.spaceplanner.model.Statement;
import fr.eql.ai110.spaceplanner.model.User;
import fr.eql.ai110.spaceplanner.repository.StatementIDao;
import fr.eql.ai110.spaceplanner.service.StatementIBusiness;

@Service
public class StatementBusiness implements StatementIBusiness {

	@Autowired
	private StatementIDao statementDao;
	
	@Override
	public Set<Statement> getStatementsByEmployee(User employee)
	{	
		return statementDao.findByEmployee(employee);
	}

	@Override
	public Long getNbStatementsByEmployee(User employee)
	{
		return statementDao.countByEmployee(employee);
	}

	@Override
	public Set<Statement> getStatementsByManager(User manager)
	{
		return statementDao.findByManager(manager);
	}

	@Override
	public Long getNbStatementsByManager(User manager)
	{
		return statementDao.countByManager(manager);
	}

	@Override
	public Long getNbStatementsWithNoValidationDateByManager(User manager)
	{
		return statementDao.countByManagerAndValidationDateIsNull(manager);
	}

	@Override
	public Set<Statement> getStatementsBeforeChoosenAttendanceDate(LocalDate choosenDate)
	{
		return statementDao.findBySelectedDayBefore(choosenDate);
	}

	@Override
	public Set<Statement> getStatementsAfterChoosenAttendanceDate(LocalDate choosenDate)
	{
		return statementDao.findBySelectedDayAfter(choosenDate);
	}

	@Override
	public Set<Statement> getStatementsBetweenChoosenAttendanceDate(LocalDate choosenDateMin,
			LocalDate choosenDateMax)
	{
		return statementDao.findBySelectedDayBetween(choosenDateMin, choosenDateMax);
	}

	@Override
	public Set<Statement> getStatementsBeforeDeclarationDate(LocalDate declarationDate)
	{
		return statementDao.findByStatementDateBefore(declarationDate);
	}

	@Override
	public Set<Statement> getStatementsAfterDeclarationDate(LocalDate declarationDate)
	{
		return statementDao.findByStatementDateAfter(declarationDate);
	}

	@Override
	public Set<Statement> getStatementsBetweenDeclarationDate(LocalDate declarationDateMin,
			LocalDate declarationDateMax)
	{
		return statementDao.findByStatementDateBetween(declarationDateMin, declarationDateMax);
	}

	@Override
	public Set<Statement> getStatementsBeforeValidationDate(LocalDate validationDate)
	{
		return statementDao.findByValidationDateBefore(validationDate);
	}

	@Override
	public Set<Statement> getStatementsAfterValidationDate(LocalDate validationDate)
	{
		return statementDao.findByValidationDateAfter(validationDate);
	}

	@Override
	public Set<Statement> getStatementsBetweenValidationDate(LocalDate validationDateMin, LocalDate validationDateMax)
	{
		return statementDao.findByValidationDateBetween(validationDateMin, validationDateMax);
	}

	@Override
	public Set<Statement> getValidatedAttendanceDates(User employee)
	{
		return statementDao.findByEmployeeAndValidationDateIsNotNullAndCancellationDateIsNull(employee);
	}

	@Override
	public Set<Statement> getStatementsToValidate(User manager)
	{	
		return statementDao.findByManagerAndValidationDateIsNullAndCancellationDateIsNotNull(manager);
	}

	@Override
	public Set<Statement> getValidatedAttendanceDatesBetweenDates(User employee, LocalDate minDate, LocalDate maxDate)
	{
		return statementDao.findValidatedAndNotCancelledSelectedDaysByEmployeeBetweenGivenDates(employee, minDate, maxDate);
	}

}
