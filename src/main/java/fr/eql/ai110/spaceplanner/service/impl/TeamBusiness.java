package fr.eql.ai110.spaceplanner.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.spaceplanner.model.Team;
import fr.eql.ai110.spaceplanner.model.User;
import fr.eql.ai110.spaceplanner.repository.TeamIDao;
import fr.eql.ai110.spaceplanner.service.TeamIBusiness;

@Service
public class TeamBusiness implements TeamIBusiness {

	@Autowired
	private TeamIDao teamDao;

	@Override
	public Team getByName(String teamName)
	{
		return teamDao.findByName(teamName);
	}

	@Override
	public Team getTeamByEmployee(User employee)
	{
		return teamDao.findByEmployees(employee);
	}
	
	@Override
	public Long getNbTeamsByManager(User manager)
	{
		return teamDao.countByManager(manager);
	}

	@Override
	public Set<Team> getManagedTeams(User connectedManager)
	{
		return teamDao.findByManager(connectedManager);
	}

}
