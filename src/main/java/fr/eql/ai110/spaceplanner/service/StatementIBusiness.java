package fr.eql.ai110.spaceplanner.service;

import java.time.LocalDate;
import java.util.Set;

import fr.eql.ai110.spaceplanner.model.Statement;
import fr.eql.ai110.spaceplanner.model.User;

public interface StatementIBusiness {

	public Set<Statement> getStatementsByEmployee(User employee);
	public Long getNbStatementsByEmployee(User employee); 
	
	public Set<Statement> getStatementsByManager(User manager);
	public Long getNbStatementsByManager(User manager);
	public Long getNbStatementsWithNoValidationDateByManager(User manager);
	
	public Set<Statement> getStatementsBeforeChoosenAttendanceDate(LocalDate choosenDate);
	public Set<Statement> getStatementsAfterChoosenAttendanceDate(LocalDate choosenDate);
	public Set<Statement> getStatementsBetweenChoosenAttendanceDate(LocalDate choosenDateMin, LocalDate choosenDateMax);

	public Set<Statement> getStatementsBeforeDeclarationDate(LocalDate declarationDate);
	public Set<Statement> getStatementsAfterDeclarationDate(LocalDate declarationDate);
	public Set<Statement> getStatementsBetweenDeclarationDate(LocalDate declarationDateMin, LocalDate declarationDateMax);
	
	public Set<Statement> getStatementsBeforeValidationDate(LocalDate validationDate);
	public Set<Statement> getStatementsAfterValidationDate(LocalDate validationDate);
	public Set<Statement> getStatementsBetweenValidationDate(LocalDate validationDateMin, LocalDate validationDateMax);
	
	public Set<Statement> getValidatedAttendanceDates(User employee);
	public Set<Statement> getStatementsToValidate (User manager);
	
	public Set<Statement> getValidatedAttendanceDatesBetweenDates(User employee, LocalDate minDate, LocalDate maxDate);
	
}
