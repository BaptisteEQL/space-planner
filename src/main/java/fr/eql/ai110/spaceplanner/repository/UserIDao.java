package fr.eql.ai110.spaceplanner.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.eql.ai110.spaceplanner.model.Statement;
import fr.eql.ai110.spaceplanner.model.Team;
import fr.eql.ai110.spaceplanner.model.User;

public interface UserIDao extends CrudRepository<User, Long> {

	@Query("SELECT u FROM User u WHERE u.login=:loginParam " + "and u.password=:passwordParam")
	User authenticate (@Param("loginParam") String login,@Param("passwordParam") String password);
	User findByLoginAndPassword(String login, String password);
	User findByNameAndSurname(String name, String surname);
	User save(User user);
	User findByLogin(String login);
	User findByName(String name);
	User findBySurname(String surname);
	User findByEmail(String email);
	//return the list of users (identified as "employees") in a team
	Set<User> findByTeam(Team team);
	//gives the number of users (identified as "employees") in team
	Long countByTeam(Team team);
	//return the user (identified as "manager") managing the team
	User findByTeams(Team team);
	//return the user (identified as "employee") who declared the statement
	User findByStatements(Statement statement);
	//return the user (identified as "manager") who has to validate the statement
	User findByStatementsToValidate(Statement statement);
	
}
