package fr.eql.ai110.spaceplanner.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.spaceplanner.model.Team;
import fr.eql.ai110.spaceplanner.model.User;

public interface TeamIDao extends CrudRepository<Team, Long> {

	Team findByName(String name);
	Team findByEmployees(User employee);
	//gives the number of teams managed by a user (identified as "manager")
	Long countByManager(User manager);
	//returns the lit of teams managed by a user (identified as "manager")
	Set<Team> findByManager(User manager);
}
