package fr.eql.ai110.spaceplanner.repository;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.eql.ai110.spaceplanner.model.Statement;
import fr.eql.ai110.spaceplanner.model.User;

public interface StatementIDao extends CrudRepository<Statement, Long> {

	Set<Statement> findByEmployee(User employee);
	Long countByEmployee(User employee);

	Set<Statement> findByManager(User manager);
	Long countByManager(User manager);
	Long countByManagerAndValidationDateIsNull(User manager);

	Set<Statement> findBySelectedDayBefore(LocalDate maxDate);
	Set<Statement> findBySelectedDayAfter(LocalDate minDate);
	Set<Statement> findBySelectedDayBetween(LocalDate minDate, LocalDate maxDate);

	Set<Statement> findByStatementDateBefore(LocalDate maxDate);
	Set<Statement> findByStatementDateAfter(LocalDate minDate);
	Set<Statement> findByStatementDateBetween(LocalDate minDate, LocalDate maxDate);

	Set<Statement> findByValidationDateBefore(LocalDate maxDate);
	Set<Statement> findByValidationDateAfter(LocalDate minDate);
	Set<Statement> findByValidationDateBetween(LocalDate minDate, LocalDate maxDate);

	Set<Statement> findByEmployeeAndValidationDateIsNotNullAndCancellationDateIsNull(User employee);

	Set<Statement> findByEmployeeAndValidationDateIsNotNullAndCancellationDateIsNullAndSelectedDayIsBetween(User employee, LocalDate minDate, LocalDate maxDate);

	Set<Statement> findBySelectedDayIsBetweenAndEmployeeIsAndValidationDateIsNotNullAndCancellationDateIsNull(LocalDate minDate, LocalDate maxDate, User employee);

	Set<Statement> findByManagerAndValidationDateIsNullAndCancellationDateIsNotNull(User manager);

	@Query(value="SELECT s.* "
			+ "FROM statement s "
			+ "LEFT JOIN user u ON u.id = s.employee_id "
			+ "WHERE s.validation_date is not null "
			+ "AND s.cancellation_date is null "
			+ "AND u.id = :employee "
			+ "AND s.selected_day>= :minDate "
			+ "AND s.selected_day<= :maxDate ", nativeQuery = true)
	Set<Statement> findValidatedAndNotCancelledSelectedDaysByEmployeeBetweenGivenDates(@Param("employee") User employee,@Param("minDate") LocalDate minDate,@Param("maxDate") LocalDate maxDate);

}
