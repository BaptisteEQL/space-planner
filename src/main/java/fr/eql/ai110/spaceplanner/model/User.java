package fr.eql.ai110.spaceplanner.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="surname")
	private String surname;
	@Column(name="email")
	private String email;
	@Column(name="inscription_date")
	private LocalDate inscriptionDate;
	@Column(name="login")
	private String login;
	@Column(name="password")
	private String password;
	@Column(name="salt")
	private String salt;
	@ManyToOne
	@JoinColumn(referencedColumnName="id")
	private Team team;
	@OneToMany(mappedBy = "manager", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Team> teams;
	@OneToMany(mappedBy = "employee", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Statement> statements;
	@OneToMany(mappedBy = "manager", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Set<Statement> statementsToValidate;
	
	public User()
	{
		
	}

	public User(Integer id, String name, String surname, String email, 
			LocalDate inscriptionDate, String login, String password, 
			String salt, Team team, Set<Team> teams, Set<Statement> statements,
			Set<Statement> statementsToValidate)
	{
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.inscriptionDate = inscriptionDate;
		this.login = login;
		this.password = password;
		this.salt = salt;
		this.team = team;
		this.teams = teams;
		this.statements = statements;
		this.statementsToValidate = statementsToValidate;
	}

	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getSurname()
	{
		return surname;
	}
	public void setSurname(String surname)
	{
		this.surname = surname;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public LocalDate getInscriptionDate()
	{
		return inscriptionDate;
	}
	public void setInscriptionDate(LocalDate inscriptionDate)
	{
		this.inscriptionDate = inscriptionDate;
	}
	public String getLogin()
	{
		return login;
	}
	public void setLogin(String login)
	{
		this.login = login;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getSalt()
	{
		return salt;
	}
	public void setSalt(String salt)
	{
		this.salt = salt;
	}
	public Team getTeam()
	{
		return team;
	}
	public void setTeam(Team team)
	{
		this.team = team;
	}
	public Set<Team> getTeams()
	{
		return teams;
	}
	public void setTeams(Set<Team> teams)
	{
		this.teams = teams;
	}
	public Set<Statement> getStatements()
	{
		return statements;
	}
	public void setStatements(Set<Statement> statements)
	{
		this.statements = statements;
	}
	public Set<Statement> getStatementsToValidate()
	{
		return statementsToValidate;
	}
	public void setStatementsToValidate(Set<Statement> statementsToValidate)
	{
		this.statementsToValidate = statementsToValidate;
	}

}
