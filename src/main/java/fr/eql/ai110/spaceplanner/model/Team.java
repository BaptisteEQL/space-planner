package fr.eql.ai110.spaceplanner.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="team")
public class Team implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@ManyToOne
	@JoinColumn(referencedColumnName="id")
	private User manager;
	@OneToMany(mappedBy = "team", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<User> employees;
	
	public Team()
	{
		
	}

	public Team(Integer id, String name, User manager, Set<User> employees)
	{
		super();
		this.id = id;
		this.name = name;
		this.manager = manager;
		this.employees = employees;
	}

	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public User getManagerr()
	{
		return manager;
	}
	public void setManager(User manager)
	{
		this.manager = manager;
	}
	public Set<User> getEmployees()
	{
		return employees;
	}
	public void setEmployees(Set<User> employees)
	{
		this.employees = employees;
	}

}
