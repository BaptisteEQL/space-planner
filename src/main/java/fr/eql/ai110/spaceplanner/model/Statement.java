package fr.eql.ai110.spaceplanner.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="statement")
public class Statement implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="selected_day")
	private LocalDate selectedDay;
	@Column(name="statement_date")
	private LocalDate statementDate;
	@Column(name="validation_date")
	private LocalDate validationDate;
	@Column(name="refusal_date")
	private LocalDate refusalDate;
	@Column(name="cancellation_date")
	private LocalDate cancellationDate;
	@ManyToOne
	@JoinColumn(referencedColumnName="id")
	private User employee;
	@ManyToOne
	@JoinColumn(referencedColumnName="id")
	private User manager;
	
	public Statement()
	{
		
	}

	public Statement(Integer id, LocalDate selectedDay, LocalDate statementDate, 
			LocalDate validationDate, LocalDate refusalDate,
			LocalDate cancellationDate, User employee, User manager)
	{
		this.id = id;
		this.selectedDay = selectedDay;
		this.statementDate = statementDate;
		this.validationDate = validationDate;
		this.refusalDate = refusalDate;
		this.cancellationDate = cancellationDate;
		this.employee = employee;
		this.manager = manager;
	}

	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public LocalDate getSelectedDay()
	{
		return selectedDay;
	}
	public void setSelectedDays(LocalDate selectedDay)
	{
		this.selectedDay = selectedDay;
	}
	public LocalDate getStatementDate()
	{
		return statementDate;
	}
	public void setStatementDate(LocalDate statementDate)
	{
		this.statementDate = statementDate;
	}
	public LocalDate getValidationDate()
	{
		return validationDate;
	}
	public void setValidationDate(LocalDate validationDate)
	{
		this.validationDate = validationDate;
	}
	public LocalDate getRefusalDate()
	{
		return refusalDate;
	}
	public void setRefusalDate(LocalDate refusalDate)
	{
		this.refusalDate = refusalDate;
	}
	public LocalDate getCancellationDate()
	{
		return cancellationDate;
	}
	public void setCancellationDate(LocalDate cancellationDate)
	{
		this.cancellationDate = cancellationDate;
	}
	public User getEmployee()
	{
		return employee;
	}
	public void setEmployee(User employee)
	{
		this.employee = employee;
	}
	public User getManager()
	{
		return manager;
	}
	public void setManager(User manager)
	{
		this.manager = manager;
	}

}
