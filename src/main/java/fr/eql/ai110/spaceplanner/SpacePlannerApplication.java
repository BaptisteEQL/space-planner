package fr.eql.ai110.spaceplanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpacePlannerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpacePlannerApplication.class, args);
	}

}
